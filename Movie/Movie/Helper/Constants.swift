// Constants.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Global Constants
enum Constants {
    static let imagePath = "https://image.tmdb.org/t/p/w500"
    static let aboutMovieCellID = "AboutMovieCellID"
    static let segmentControlItems = ["Top", "Popular", "UpComing"]
    static let coderInitError = "init(coder:) has not been implemented"

    enum Text {
        static let empty = ""
    }

    enum SystemImage {
        static let placeholder = "photo"
    }
}
