// Results.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Model List Movies
struct Results: Decodable {
    let movies: [Movie]

    enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}
